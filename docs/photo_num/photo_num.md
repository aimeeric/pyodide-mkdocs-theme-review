---
author: Eric AIME
title: La photographie numérique (1)
tags:
  - photographie
  - résolution
  - définition
  - couleur
---


<iframe width="1098" height="617" src="https://www.youtube.com/embed/UnNPNc-F9ks" title="MOOC SNT / Photographie numérique, du réel aux pixels ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


!!! info "Questions 1"

    répondre aux questions suivantes [ici](https://parcours.algorea.org/fr/a/1943397278515775575;p=4702,1067253748629066205,1625996270397195025,1089626022569595539;pa=0)


# Définition, résolution, couleurs d'une image numérique

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ycx9tWZNRQI" title="Le capteur photo numérique, qu&#39;est ce que c&#39;est ? #01Focus" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


!!! info "En résumé"
    === "Quadrillage"
        Une image numérique se présente sous la forme d’un quadrillage - ou d'un tableau - dont chaque case est un pixel d’une couleur donnée. On peut donc repérer chaque pixel par sa ligne et sa colonne dans ce quadrillage, à l'aide de coordonnées en partant du coin en haut à gauche.

        ![image quadrillage](img/quadrillage.png){ width=65% }

    === "Codage des couleurs"
        Chaque pixel correspond à un triplet de trois nombres entiers, soit les valeurs de rouge (Red), de vert (Green) et de bleu (Blue) afin de reconstituer la couleur. Chaque valeur est codée entre 0 et 255 (ou en pourcentages, ou en hexadécimal, voir ici). On parle de code RGB (RVB in french).

        Notez bien qu'on donne d'abord la colonne puis l'abscisse d'un pixel.

        ![image quadrillage](img/frise_RBG.png){ width=165% }

    === "Définition d'une image"
        La définition de l’image est le nombre total de pixels qui la composent. Celle-ci n’est pas forcément égale à la définition du capteur.

        On l'obtient donc en multipliant sa largeur par sa hauteur. Par exemple, une image de 1920 pixels de largeur sur 1080 pixels de hauteur a une définition de 1920 x 1080 = 2073600 pixels soit à peu près 2 millions de pixels.


    === "Résolution d'une image"
        La résolution de l’image, c’est-à-dire le nombre de pixels par unité de longueur, détermine sa qualité à l’impression ou sur un écran.

        Par exemple, la résolution standard pour affichage sur le web est de 72 ppp (pixels par pouce) alors qu'une résolution de 300 ppp est recommandée pour l'impression.

!!! info "Questions 2"
    [Exercice sur définition et résolution](https://parcours.algorea.org/fr/a/349914841333683613;p=4702,1067253748629066205,1625996270397195025,1089626022569595539;pa=0)


!!! info "Questions 3"
    [Exercice sur les couleurs](https://parcours.algorea.org/fr/a/363475139505379105;p=4702,1067253748629066205,1625996270397195025,1089626022569595539;pa=0)

# Encodage des informations d'une image - compression des données

!!! info "Encodage d'une image"
    [Comprendre comment on peut coder les pixels d'une image](https://parcours.algorea.org/fr/a/817625730839546105;p=4702,1067253748629066205,1625996270397195025,1089626022569595539;a=0){:target="_blank" }

!!! info "Questions 4 : quizz sur encodage d'une image"
    Répondre au [quizz suivant](https://parcours.algorea.org/fr/a/1595013240262378229;p=4702,1067253748629066205,1625996270397195025,1089626022569595539;a=0) sur l'encdage des images.

# Couleurs d'une image 

!!! info "utilisation de l'outil Pipette de Gimp"
    Gimp est un logiciel très puissant et gratuit permettant de faire des retouches d'images numériques. 
    ![image quadrillage](img/outil_pipette.png){ width=25% : .center }

!!! info "Activité couleurs avec Gimp"
    [ici](https://parcours.algorea.org/fr/a/40290561495812901;p=4702,1067253748629066205,1625996270397195025,1680805270915318251,480514161722644174;pa=0){:target="_blank"}

!!! info "Questions 4"
    [Exercice sur les couleurs](https://parcours.algorea.org/fr/a/363475139505379105;p=4702,1067253748629066205,1625996270397195025,1089626022569595539;pa=0)


# En conclusion 
!!! abstract "Résumé"
    * Pour représenter numériquement une image on la découpe en une grille ou matrice de pixels (picture element) et on associe à chaque pixel une valeur numérique. On parle de **représentation bitmap** (matrice de bits, les bits permettant de stocker des nombres) d'une image.

    * La **définition d'une image** est le nombre de pixels qui composent l'image.

    * La **résolution d'une image** est le nombre de pixels par unité de longueur. On l'exprime en général en ppp(pixels par pouce ou dots per inch (dpi) en anglais, le pouce est une unité de longueur anglo-saxonne mesurant cm). La résolution standard pour le Web est de 72 ppp et pour une impression de 300 ppp. 


![image quadrillage](img/poisson.png){ width=65% }

Cette image possède une définition de 60px et un résolution de 3ppp.

